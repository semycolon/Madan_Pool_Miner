package me.semycolon.madan;

public final class C0035R {

    public static final class array {
        public static final int algorithms_array = 2131165184;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int ic_launcher = 2130837504;
        public static final int notification_icon = 2130837505;
    }

    public static final class id {
        public static final int LinearLayout02 = 2131296266;
        public static final int button1 = 2131296267;
        public static final int button2 = 2131296268;
        public static final int editText = 2131296259;
        public static final int editText1 = 2131296258;
        public static final int exit = 2131296272;
        public static final int linar1 = 2131296257;
        public static final int password1 = 2131296261;
        public static final int relativeLayoutOuter = 2131296256;
        public static final int scrollView1 = 2131296270;
        public static final int spinner = 2131296265;
        public static final int spinner1 = 2131296264;
        public static final int textView = 2131296263;
        public static final int textView1 = 2131296271;
        public static final int textView4 = 2131296262;
        public static final int textView5 = 2131296269;
        public static final int username1 = 2131296260;
    }

    public static final class layout {
        public static final int main = 2130968576;
    }

    public static final class menu {
        public static final int menu = 2131230720;
    }

    public static final class mipmap {
        public static final int ic_launcher = 2130903040;
    }

    public static final class raw {
        public static final int minerd = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int hello = 2131099648;
    }
}
