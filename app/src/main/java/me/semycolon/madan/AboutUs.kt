package me.semycolon.madan

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_about_us.*

class AboutUs : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        about_developer_name.text= "سید حسن عاشقی" + "\n " + "SemyColon.me"
        about_developer_name.setOnClickListener {
            try {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.data = Uri.parse("http://SemyColon.me")
                startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        about_developer_mail.text= "info@SemyColon.me"
        about_developer_mail.setOnClickListener {
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, "info@semycolon.me")
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "سانگ هاب")
            //emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here")

            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."))
                finish()
            } catch (ex: android.content.ActivityNotFoundException) {
                Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show()
            }

        }


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            finish()
        }
        return true
    }
}
