package me.semycolon.madan;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



public class MinerActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "Miner";
    private static final byte[] SALT = new byte[]{(byte) 3, (byte) -14, (byte) 15, (byte) -92, (byte) -65, (byte) 35, (byte) 89, (byte) -79, (byte) 32, (byte) -38, (byte) 46, (byte) 26, (byte) -43, (byte) -38, (byte) -34, (byte) 113, (byte) 11, (byte) -32, (byte) 64, (byte) -89};
    private static final String TAG = "DroidBTCManagerActivity";

    private static final DateFormat logDateFormat = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss] ");
    public final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAogwSPUbiiafJmn6LEiqX2Cz++eJv47B6CWr7b2FPY1OTkCVj89XMJ0zkf4XlDFCVvWL66Vqg09OfjFVfHptaCS9y+gwAc6d4iWBdzkubixcKD76GhZ1GtClTZ0Cq1QdpCSpD5h5b3Srd9VaJqG130wOg15sXo278WPWjJRkEZP247eG0MjhKm0qtXBH75uxGdAjfALrtIvYh+CF2f27/kaeqzt8SR1tBjjZ5TZr1HqUSvDle2wzNpb9StMnlaNPjqGKxJ5JGcNhQdkbgUy8oK/XTCD0Q+wGi8xn12PgPq9QxUCt7IS0qlGBKroRclBSsYgvYNVxa4033orf1hNYv9QIDAQAB";
    EditText Cred;
    String CredText;
    public final String DEFAULT_TYPE = "ARM (default)";
    private String Minerd;
    EditText URL;
    String URLText;
    String algo;
    Spinner algoList;
    public String currentLine;
    String deviceId;
    public boolean downloaded;
    EditText ePassword;
    EditText eUser;
    SharedPreferences.Editor editor;
    boolean finished;
    double[] hashrate;
//    LicenseChecker mChecker;
    private Handler mHandler;
    public final int mId = 314;
//    private LicenseCheckerCallback mLicenseCheckerCallback;
    NotificationManager mNotificationManager;
    ProgressDialog mProgressDialog;
    String minerd;
    String minerdType;
    Notification note;
    public boolean noteIsOn;
    NotificationCompat.Builder notebuilder;
    ProgressDialog f1p;
    EditText port;
    private Process process;
    RadioButton rBitcoin;
    RadioButton rLitecoin;
    Intent resultIntent;
    Runnable runnable = new C00404();
    SharedPreferences settings;
    Spinner sminerdType;
    Button startMining;
    Button stopMining;
    String str1;
    Activity thisActivity;
    Spinner threadList;
    BigDecimal totalHashrate;
    String typeMinerd;
    Void f2v;
    PowerManager.WakeLock wakelock;

    class C00361 implements DialogInterface.OnClickListener {
        C00361() {
        }

        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -1:
                    if (MinerActivity.this.process != null) {
                        MinerActivity.this.process.destroy();
                    }
                    if (MinerActivity.this.noteIsOn) {
                        MinerActivity.this.mNotificationManager.cancel(314);
                    }
                    MinerActivity.this.finish();
                    System.exit(0);
                    return;
                default:
                    return;
            }
        }
    }

    class C00372 implements View.OnClickListener {
        C00372() {
        }

        public void onClick(View v) {
            int i = 1;
            Spinner threadList = (Spinner) MinerActivity.this.findViewById(R.id.spinner1);
            MinerActivity.this.URL = (EditText) MinerActivity.this.findViewById(R.id.editText1);
            ((TextView) MinerActivity.this.findViewById(R.id.textView1)).setText("");
            MinerActivity.this.URLText = MinerActivity.this.URL.getText().toString();
            MinerActivity.this.CredText = MinerActivity.this.eUser.getText().toString() + ":" + MinerActivity.this.ePassword.getText().toString();
            boolean abc = MinerActivity.this.checkMinerd(MinerActivity.this.minerd);
            MinerActivity.this.wakelock.acquire();
            MinerActivity.this.algo = MinerActivity.this.algoList.getSelectedItem().toString();
            MinerActivity.this.disableUi();
            Log.i("DroidBtc.UI", "UI elements disabled");
            int i2 = MinerActivity.this.eUser.getText().toString() == "" ? 1 : 0;
            if (MinerActivity.this.ePassword.getText().toString() != "") {
                i = 0;
            }
            if ((i2 | i) != 0) {
                Toast.makeText(MinerActivity.this.getApplicationContext(), "یوزرنیم و پسوورد نباید خالی باشد", Toast.LENGTH_SHORT).show();
                try {
                    MinerActivity.this.process.destroy();
                    MinerActivity.this.enableUi();
                } catch (Exception e) {
                }
            }
        }
    }

    class C00383 implements View.OnClickListener {
        C00383() {
        }

        public void onClick(View v) {
            try {
                MinerActivity.this.process.destroy();
                MinerActivity.this.enableUi();
                MinerActivity.this.port.setEnabled(true);
                MinerActivity.this.wakelock.release();
            } catch (Exception e) {
            }
        }
    }

    class C00404 implements Runnable {

        class C00391 implements DialogInterface.OnClickListener {
            C00391() {
            }

            public void onClick(DialogInterface dialog, int which) {
                MinerActivity.this.finish();
            }
        }

        C00404() {
        }

        public void run() {
            if (MinerActivity.this.isOnline()) {
                MinerActivity.this.setThreads();
                MinerActivity.this.setUI();
                return;
            }
            AlertDialog alertDialog = new AlertDialog.Builder(MinerActivity.this).create();
            alertDialog.setTitle("No Internet Connection");
            alertDialog.setMessage("An Internet connection is required to mine.");
            alertDialog.setButton("OK", new C00391());
            alertDialog.show();
        }
    }

    class C00415 implements DialogInterface.OnClickListener {
        C00415() {
        }

        public void onClick(DialogInterface dialog, int id) {
            MinerActivity.this.log(MinerActivity.logDateFormat.format(new Date()) + String.format("%1$s doesn't exist. Downloading...", new Object[]{MinerActivity.this.Minerd}));
            DownloadFile a = new DownloadFile();
            MinerActivity.this.finished = false;
            a.execute(new Void[0]);
            MinerActivity.this.typeMinerd = "minerd";
        }
    }

    class C00426 implements DialogInterface.OnClickListener {
        C00426() {
        }

        public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
            try {
                if (MinerActivity.this.process != null) {
                    MinerActivity.this.process.destroy();
                }
                MinerActivity.this.wakelock.release();
            } catch (Exception e) {
            }
        }
    }

    class C00469 implements Runnable {
        C00469() {
        }

        public void run() {
            MinerActivity.this.thisActivity.setTitle(MinerActivity.this.str1);
        }
    }

    private class DownloadFile extends AsyncTask<Void, Integer, String> {
        private DownloadFile() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            MinerActivity.this.mProgressDialog.setCancelable(false);
            MinerActivity.this.mProgressDialog.show();
        }

        protected String doInBackground(Void... void1) {
            try {
                InputStream input = MinerActivity.this.getResources().openRawResource(R.raw.minerd);
                int fileLength = input.available();
                OutputStream output = new FileOutputStream(String.format("%1$s/%2$s", new Object[]{MinerActivity.this.appPath(), MinerActivity.this.typeMinerd}));
                byte[] data = new byte[1024];
                long total = 0;
                while (true) {
                    int count = input.read(data);
                    if (count != -1) {
                        total += (long) count;
                        publishProgress(new Integer[]{Integer.valueOf((int) ((100 * total) / ((long) fileLength)))});
                        output.write(data, 0, count);
                    } else {
                        output.flush();
                        output.close();
                        input.close();
                        MinerActivity.this.runCommand(String.format("chmod 744 %1$s/%2$s", new Object[]{MinerActivity.this.appPath(), MinerActivity.this.typeMinerd}));
                        return "poop";
                    }
                }
            } catch (Exception e) {
                MinerActivity.this.finished = true;
                return "poop";
            }
        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            MinerActivity.this.mProgressDialog.setProgress(progress[0].intValue());
        }

        protected void onPostExecute(String result) {
            int i;
            super.onPostExecute(result);
            MinerActivity.this.mProgressDialog.cancel();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                if (e.getMessage() != null) {
                    i = 1;
                } else {
                    i = 0;
                }
                if ((i | (e.getMessage() != "" ? 1 : 0)) != 0) {
                    Log.i("DROIDBTC EXCEPTION", e.getMessage());
                }
            }
            MinerActivity.this.startMiner(MinerActivity.this.URLText, MinerActivity.this.CredText, MinerActivity.this.threadList.getSelectedItem().toString());
            MinerActivity.this.startMining.setEnabled(false);
            MinerActivity.this.stopMining.setEnabled(true);
            MinerActivity.this.editor.putString("username", MinerActivity.this.eUser.getText().toString());
            MinerActivity.this.editor.putString("password", MinerActivity.this.ePassword.getText().toString());
            MinerActivity.this.editor.putString("url", MinerActivity.this.URL.getText().toString());
            MinerActivity.this.editor.commit();
            MinerActivity.this.showNote();
        }
    }

    /*
    private class MyLicenseCheckerCallback implements LicenseCheckerCallback {

        class C00471 implements Runnable {
            C00471() {
            }

            public void run() {
                Toast.makeText(MinerActivity.this.getApplicationContext(), "You are a pirate. Please cease the usage of my app.", Toast.LENGTH_SHORT).show();
                MinerActivity.this.finish();
                System.exit(0);
            }
        }

        private MyLicenseCheckerCallback() {
        }

        public void allow(int reason) {
            if (!MinerActivity.this.isFinishing()) {
                displayResult("License verified.");
            }
        }

        public void dontAllow(int reason) {
            if (!MinerActivity.this.isFinishing()) {
                if (reason == Policy.RETRY) {
                    displayResult("License not verified. Please try again.");
                    return;
                }
                displayResult("License not verified. You are a pirate. Go away.");
                MinerActivity.this.mHandler.post(new C00471());
            }
        }

        private void displayResult(final String result) {
            MinerActivity.this.mHandler.post(new Runnable() {
                public void run() {
                    Toast.makeText(MinerActivity.this.getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                }
            });
        }

        public void applicationError(int errorCode) {
        }
    }

    */
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.miner, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_exit:
                DialogInterface.OnClickListener dialogClickListener = new C00361();
                AlertDialog dialog = new AlertDialog.Builder(this).setMessage("ایا میخواهید خارج شوید؟").setPositiveButton("آره", dialogClickListener).setNegativeButton("خیر", dialogClickListener).create();
                dialog.show();
                break;
            case R.id.menu_about:
                String about = "developed by : SemyColon.me\nSemyColon";
                Toast.makeText(thisActivity, about, Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);


        this.hashrate = new double[8];
        for (double b : this.hashrate) {
        }
        this.totalHashrate = new BigDecimal(0);
        this.threadList = (Spinner) findViewById(R.id.spinner1);
        this.deviceId = Settings.Secure.getString(getContentResolver(), "android_id");
        this.wakelock = ((PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE)).newWakeLock(536870918, TAG);
        this.thisActivity = this;
        this.mHandler = new Handler();
        this.eUser = (EditText) findViewById(R.id.username1);
        this.ePassword = (EditText) findViewById(R.id.password1);
        this.f2v = null;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miner);
        this.currentLine = "";
        this.noteIsOn = false;
        this.settings = getSharedPreferences(PREFS_NAME, 0);
//        this.mChecker = new LicenseChecker(this, new ServerManagedPolicy(this, new AESObfuscator(SALT, getPackageName(), this.deviceId)), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAogwSPUbiiafJmn6LEiqX2Cz++eJv47B6CWr7b2FPY1OTkCVj89XMJ0zkf4XlDFCVvWL66Vqg09OfjFVfHptaCS9y+gwAc6d4iWBdzkubixcKD76GhZ1GtClTZ0Cq1QdpCSpD5h5b3Srd9VaJqG130wOg15sXo278WPWjJRkEZP247eG0MjhKm0qtXBH75uxGdAjfALrtIvYh+CF2f27/kaeqzt8SR1tBjjZ5TZr1HqUSvDle2wzNpb9StMnlaNPjqGKxJ5JGcNhQdkbgUy8oK/XTCD0Q+wGi8xn12PgPq9QxUCt7IS0qlGBKroRclBSsYgvYNVxa4033orf1hNYv9QIDAQAB");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.mProgressDialog = new ProgressDialog(this);
        this.mProgressDialog.setMessage("Extracting miner...");
        this.mProgressDialog.setIndeterminate(false);
        this.mProgressDialog.setMax(100);
        this.mProgressDialog.setProgressStyle(1);
        setContentView(R.layout.activity_miner);
        this.notebuilder = new NotificationCompat.Builder(getApplicationContext()).setContentTitle(getString(R.string.app_name)).setContentText("درحال ماین کردن ...").setSmallIcon(R.drawable.miner).setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.miner));
        this.editor = this.settings.edit();
        new Handler().postDelayed(this.runnable, 1);
        ((EditText) findViewById(R.id.editText1)).setText(this.settings.getString("url", ""));
        EditText user = (EditText) findViewById(R.id.username1);
        EditText pass = (EditText) findViewById(R.id.password1);
        user.setText(this.settings.getString("username", ""));
        pass.setText(this.settings.getString("password", ""));
        EditText port = (EditText) findViewById(R.id.editText);
        int abc = this.settings.getInt("port", 0);
        if (abc != 0) {
            port.setText(Integer.toString(abc));
        }
        this.eUser = user;
        this.ePassword = pass;
        this.algoList = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.algorithms_array, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.algoList.setAdapter(adapter);
    }

    String appPath() {
        String s = null;
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).applicationInfo.dataDir;
        } catch (Exception ex) {
            log(ex.getMessage());
            return s;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            this.process.destroy();
        } catch (Exception e) {
        }
    }

    void setThreads() {
        try {
            this.threadList = (Spinner) findViewById(R.id.spinner1);
            String[] threadsAvailable = new String[8];
            for (int i = 0; i <= 8; i++) {
                threadsAvailable[i] = Integer.toString(i + 1);
                ArrayAdapter threads = new ArrayAdapter(this, android.R.layout.simple_list_item_1, threadsAvailable);
                threads.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                this.threadList.setAdapter(threads);
            }
        } catch (Exception e) {
        }
    }

    void setUI() {
        this.startMining = (Button) findViewById(R.id.button1);
        this.stopMining = (Button) findViewById(R.id.button2);
        this.startMining.setOnClickListener(new C00372());
        this.stopMining.setOnClickListener(new C00383());
    }

    boolean isOnline() {
        NetworkInfo netInfo = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
            return false;
        }
        return true;
    }

    void startMiner(String URL, String Auth, String Threads) {
        main(new String[]{URL, Auth, Threads});
    }

    boolean checkMinerd(String minerdType) {
        this.Minerd = "minerd";
        try {
            if (new File(appPath() + String.format("/%1$s", new Object[]{this.Minerd})).exists()) {
                log(logDateFormat.format(new Date()) + String.format("%1$s is present.", new Object[]{this.Minerd}));
                startMiner(this.URLText, this.CredText, this.threadList.getSelectedItem().toString());
                this.startMining.setEnabled(false);
                this.stopMining.setEnabled(true);
                this.editor.putString("username", this.eUser.getText().toString());
                this.editor.putString("password", this.ePassword.getText().toString());
                this.editor.putString("url", this.URL.getText().toString());
                this.editor.commit();
                showNote();
                return true;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this.thisActivity);
            builder.setPositiveButton("Yes", new C00415());
            builder.setNegativeButton("No", new C00426());
            builder.setMessage("Miner not extracted. Extract miner?");
            builder.create().show();
            return false;
        } catch (Exception e) {
            log(e.getMessage());
            return false;
        }
    }

    void Miner(String minerdType, final String url, final String auth, final int nThread) {
        this.Minerd = "minerd";
        new Thread() {
            public void run() {
                int port;
                String url1;
                try {
                    EditText e = (EditText) MinerActivity.this.findViewById(R.id.editText);
                    try {
                        port = Integer.parseInt(e.getText().toString());
                    } catch (Exception e2) {
                        e.setText("3333");
                        port = 3333;
                    }
                    try {
                        if (url.toLowerCase().contains("://")) {
                            url1 = InetAddress.getByName(url.split("://")[1]).getHostAddress();
                        } else {
                            url1 = InetAddress.getByName(url).getHostAddress();
                        }
                    } catch (UnknownHostException f) {
                        Log.i(MinerActivity.TAG, f.getMessage());
                        url1 = "";
                    }
                    MinerActivity.this.editor.putInt("port", port);
                    MinerActivity.this.editor.commit();
                } catch (Exception e1) {
                    e1.printStackTrace();
                    Toast.makeText(MinerActivity.this, "خطایی رخ داده است!!", Toast.LENGTH_SHORT).show();
                    return;
                }
                try {
                    MinerActivity andLTCMinerPROActivity = MinerActivity.this;
                    ProcessBuilder directory = new ProcessBuilder(new String[0]).directory(new File(MinerActivity.this.appPath()));
                    String[] r16 = new String[5];
                    r16[0] = String.format("./%1$s", new Object[]{MinerActivity.this.Minerd});
                    r16[1] = String.format("--algo=%1$s", new Object[]{MinerActivity.this.algoList.getSelectedItem().toString()});
                    r16[2] = String.format("--url=stratum+tcp://%1$s:%2$s", new Object[]{url1, Integer.toString(port)});
                    r16[3] = String.format("--userpass=%1$s", new Object[]{auth});
                    r16[4] = String.format("--threads=%1$s", new Object[]{Integer.toString(nThread)});
                    andLTCMinerPROActivity.process = directory.command(r16).redirectErrorStream(true).start();
                    BufferedReader ins = new BufferedReader(new InputStreamReader(MinerActivity.this.process.getInputStream()));
                    while (true) {
                        String line = ins.readLine();
                        if (line != null) {
                            MinerActivity.this.log(line);
                        } else {
                            MinerActivity.this.process.destroy();
                            return;
                        }
                    }
                } catch (Exception ex) {
                    MinerActivity.this.log(ex.getMessage());
                } catch (Throwable th) {
                    MinerActivity.this.process.destroy();
                }
            }
        }.start();
    }

    void Download(String URL, String fileName) {
        try {
            java.net.URL url = new URL(URL);
            File file = new File(fileName);
            BufferedInputStream bis = new BufferedInputStream(url.openConnection().getInputStream());
            ByteArrayBuffer baf = new ByteArrayBuffer(50);
            while (true) {
                int current = bis.read();
                if (current != -1) {
                    baf.append((byte) current);
                } else {
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(baf.toByteArray());
                    fos.close();
                    return;
                }
            }
        } catch (IOException e) {
            log("Error: " + e);
        }
    }

    public void main(String[] args) {
        String url = null;
        String auth = null;
        int nThread = 1;
        this.minerdType = null;
        if (args.length > 0) {
            url = args[0];
        }
        if (args.length > 1) {
            auth = args[1];
        }
        if (args.length > 2) {
            nThread = Integer.parseInt(args[2]);
        }
        if (args.length > 3) {
            this.minerdType = args[3];
        }
        try {
            Miner(this.minerdType, url, auth, nThread);
        } catch (Exception e) {
            log(e.getMessage());
        }
    }

    void log(final String str) {
        int i;
        int i2;
        if (str == null) {
            i = 1;
        } else {
            i = 0;
        }
        if (str == "") {
            i2 = 1;
        } else {
            i2 = 0;
        }
        if ((i | i2) == 0) {
            Log.i("DroidBtc.Log", str);
            runOnUiThread(new Runnable() {
                public void run() {
                    TextView Console = (TextView) MinerActivity.this.findViewById(R.id.textView1);
                    Console.setTextColor(-16777216);
                    Console.append(str + "\n");
                    final ScrollView scrollview = (ScrollView) MinerActivity.this.findViewById(R.id.scrollView1);
                    scrollview.post(new Runnable() {
                        public void run() {
                            scrollview.fullScroll(130);
                        }
                    });
                }
            });
            if (this.noteIsOn) {
                if (str.contains("khash/s")) {
                    this.str1 = String.format("%.2f", new Object[]{Double.valueOf(parseHashrate(str))}) + " khash/s";
                    this.thisActivity.runOnUiThread(new C00469());
                } else {
                    this.str1 = str;
                }
                this.mNotificationManager.notify(314, this.notebuilder.setContentText(this.str1).build());
            }
            if (str.contains("minerd --help")) {
                log("Failed to start miner.");
            }
        }
    }

    void runCommand(String command) {
        try {
            Runtime.getRuntime().exec(command);
        } catch (IOException ex) {
            log(ex.getMessage());
        }
    }

    public void onStop(Bundle bundle) {
        if (this.process != null) {
            this.process = null;
        }
        super.onStop();
    }

    public void showNote() {
        this.noteIsOn = true;
        this.resultIntent = new Intent(this, MinerActivity.class);
        Intent toLaunch = new Intent(getApplicationContext(), MinerActivity.class);
        toLaunch.setAction("android.intent.action.MAIN");
        PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT);
        this.resultIntent.setAction("android.intent.action.MAIN");
        this.resultIntent.addCategory("android.intent.category.LAUNCHER");
        this.notebuilder.setContentIntent(resultPendingIntent);
        this.notebuilder.setOngoing(true);
        this.note = this.notebuilder.build();
        Notification notification = this.note;
        notification.flags |= 2;
        this.mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        this.mNotificationManager.notify(314, this.note);
    }

    public void disableUi() {
        this.URL.setEnabled(false);
        this.eUser.setEnabled(false);
        this.ePassword.setEnabled(false);
        ((EditText) findViewById(R.id.editText)).setEnabled(false);
        this.threadList.setEnabled(false);
        this.algoList.setEnabled(false);
    }

    public void enableUi() {
        this.startMining.setEnabled(true);
        this.stopMining.setEnabled(false);
        log(logDateFormat.format(new Date()) + "Stopping miner...");
        this.mNotificationManager.cancel(314);
        this.noteIsOn = false;
        this.URL.setEnabled(true);
        this.eUser.setEnabled(true);
        this.ePassword.setEnabled(true);
        this.algoList.setEnabled(true);
        this.threadList.setEnabled(true);
        ((EditText) findViewById(R.id.editText)).setEnabled(true);
        this.thisActivity.runOnUiThread(new Runnable() {
            public void run() {
                MinerActivity.this.thisActivity.setTitle(getString(R.string.app_name));
            }
        });
    }

    public double parseHashrate(String str) {
        BigDecimal abc = new BigDecimal(0);
        String a4 = str.split(",")[1].split("k")[0].replaceAll(" ", "");
        if (str.contains("thread 0")) {
            this.hashrate[0] = Double.parseDouble(a4);
        }
        if (str.contains("thread 1")) {
            this.hashrate[1] = Double.parseDouble(a4);
        }
        if (str.contains("thread 2")) {
            this.hashrate[2] = Double.parseDouble(a4);
        }
        if (str.contains("thread 3")) {
            this.hashrate[3] = Double.parseDouble(a4);
        }
        if (str.contains("thread 4")) {
            this.hashrate[4] = Double.parseDouble(a4);
        }
        if (str.contains("thread 5")) {
            this.hashrate[5] = Double.parseDouble(a4);
        }
        if (str.contains("thread 6")) {
            this.hashrate[6] = Double.parseDouble(a4);
        }
        if (str.contains("thread 7")) {
            this.hashrate[7] = Double.parseDouble(a4);
        }
        double sum = 0.0d;
        for (double d : this.hashrate) {
            sum += d;
        }
        abc = BigDecimal.valueOf(sum);
        Log.i("MultiMiner hashrate ", a4 + " khash/s - this is the string hashrate");
        Log.i("MultiMiner hashrate ", sum + " khash/s - this is the double hashrate");
        Log.i("MultiMiner hashrate ", this.totalHashrate.toString() + " khash/s - this is the BigDecimal hashrate");
        return sum;
    }
}